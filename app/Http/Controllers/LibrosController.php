<?php

namespace App\Http\Controllers;

use App\Libro;
use Illuminate\Http\Request;

class LibrosController extends Controller
{

    public function index()
    {
        return view('libreria');
    }

    public function traerLibros(Request $request)
    {

        $filtro = $request->buscador;

        $libros = Libro::filtroPorTituloYAutor( $filtro )->get();

        return response()->json($libros, 200);

    }

}
