@extends('layouts.principal')
@section('styles')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">

    <style>

        .nav-principal {
            box-shadow: 0 8px 25px rgba(88,88,88,0.19), 0 4px 6px rgba(88,88,88,0.23);
        }

        .logo {
            width: 75px;
        }

        .bg-light {
            background-color: #fff !important;
        }

        .buscador {
            width: 400px !important;
        }

        .btn-registrate {
            border: 1px solid #707070;
            border-radius: 5px;
            padding: 7px 15px !important;
        }

        .img-portada-libro {
            width: 100px;
            height: 160px;
            margin: 0 auto;
            object-fit: cover;
        }

        .precio-libro {
            font-size: 1.4rem;
        }

        .titulo-libro {
            font-size: 1.5rem;
        }

        .estrella-completa {
            color: #FFD813;
        }

        .enlace-libro:hover {
            text-decoration: none;
        }

        .enlace-libro,
        .enlace-libro:hover{
            color: #2E2E2E;
        }

        .enlace-libro:hover h2 {
            text-decoration: underline;
        }

    </style>

@endsection
@section('contenido')

    <nav class="navbar navbar-expand-lg navbar-light bg-light nav-principal">
        <div class="container">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-between align-items-center" id="navbarTogglerDemo03">

                <a class="navbar-brand" href="#">
                    <img src="{{ asset('images/logo.png') }}" class="logo" alt="">
                </a>

                <form class="form-inline my-2 my-lg-0">
                    <input
                        class="form-control mr-sm-2 buscador"
                        type="search"
                        placeholder="Buscar por autor, nombre del libro..."
                        aria-label="Search"
                        v-model="buscador"
                        @keyup="buscarLibros"
                    >
                </form>

                <ul class="navbar-nav mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
                    </li>
                    <li class="nav-item ml-2">
                        <a class="nav-link btn-registrate" href="#">
                            Regístrate
                        </a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>


    <section class="container mt-4">

        <div class="row">

            <div class="col-md-3 mb-4" v-for="libro in libros">

                <a href="#detalle" class="enlace-libro">
                    <div class="card card-body text-center">

                        <img :src="'/' + libro.foto_portada " class="img-portada-libro" alt="">
                        <h2 class="font-weight-bold mt-3 titulo-libro" v-text="libro.titulo"></h2>
                        <p>
                            <span v-text="libro.autor"></span> <br>
                            <span>
                            <i class='bx bxs-star estrella-completa'></i>
                            <i class='bx bxs-star estrella-completa'></i>
                            <i class='bx bxs-star estrella-completa'></i>
                            <i class='bx bxs-star estrella-completa'></i>
                            <i class='bx bxs-star estrella-completa'></i>
                        </span>
                        </p>

                        <strong class="precio-libro">
                            $ <span v-text="libro.precio"></span> COP
                        </strong>

                    </div>
                </a>

            </div>

        </div>


    </section>

@endsection
@section('scripts')

    <script src="{{ asset('js/libreria/listado_libros.js') }}"></script>

@endsection
