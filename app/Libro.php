<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{

    // Timestamps
    public $timestamps = false;

    // Guarded
    protected $guarded = [];

    public function scopeFiltroPorTituloYAutor($query, $filtro)
    {
        if( !empty( $filtro) ) {
            $query->where('titulo', 'LIKE', '%'.$filtro.'%')
                ->orWhere('autor', 'LIKE', '%'.$filtro.'%');
        }
    }

}
