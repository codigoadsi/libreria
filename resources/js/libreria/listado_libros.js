
const app = new Vue({
    el: '#app',
    created() {

        this.traerLibros();

    },
    data: {
        libros: [],
        buscador: '',
        setTimeoutBuscador: ''
    },
    methods: {

        traerLibros() {

            axios.get('/traer-libros', {
                params: {
                    buscador: this.buscador
                }
            })
                .then( res => {
                    this.libros = res.data;
                })
                .catch( error => {
                    console.log( error.response )
                });


        },

        buscarLibros() {

            clearTimeout( this.setTimeoutBuscador )
            this.setTimeoutBuscador = setTimeout(this.traerLibros, 360)

        }

    }
});
