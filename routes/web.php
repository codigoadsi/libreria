<?php


Route::post('/validacion-iniciar-sesion', 'ValidacionIniciarSesionController@iniciar_sesion');
Route::post('/registro', 'ValidacionIniciarSesionController@registro')->name('registro');


Route::get('/', 'LibrosController@index')->name('index');
Route::get('/traer-libros', 'LibrosController@traerLibros');


Route::middleware('guest')->group( function() {

    Route::get('/iniciar-sesion', function () {
        return view('iniciar_sesion');
    })->name('login');

    Route::get('/registro', function () {
        return view('registro');
    })->name('registro');

});

Route::middleware(['auth', 'administrativo'])->group( function() {

    Route::get('/panel-administrativo', 'VistasAdministrativoController@index');

});


Route::middleware(['auth', 'estandar'])->group( function() {

    Route::get('/panel-estandar', 'VistasEstandarController@index');

});

Route::get('/cerrar-sesion', function() {

    // Cerrar la sesión
    Auth::logout();

    // Redireccionando
    return redirect()->route('login');
});

